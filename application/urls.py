from django.urls import path
from . import views

urlpatterns = [
    path('api_list/', views.index, name='apiDoc'),

    path('api/v1/devices/', views.GetDevices.as_view(), name='getAllDevices'),
    path('api/v1/devices/<int:pk>/', views.DevicesRUD.as_view(), name='getSingleDevice'),
    path('api/v1/devices/create_device/', views.CreateDevice.as_view(), name='createDevice'),
    path('api/v1/devices/<int:pk>/update_device/', views.DevicesRUD.as_view(), name='updateDevice'),
    path('api/v1/devices/<int:pk>/delete_device/', views.DevicesRUD.as_view(), name='deleteDevice'),
    ##
    path('api/v1/properties/', views.GetProperties.as_view(), name='getAllProperties'),
    path('api/v1/properties/<int:pk>/', views.PropertiesRUD.as_view(), name='getSingleProperty'),
    path('api/v1/properties/create_property/', views.CreateProperty.as_view(), name='createProperty'),
    path('api/v1/properties/<int:pk>/update_property/', views.PropertiesRUD.as_view(), name='updateProperty'),
    path('api/v1/properties/<int:pk>/delete_property/', views.PropertiesRUD.as_view(), name='deleteProperty'),
    ##
    path('api/v1/devices/<int:device_id>/properties/', views.DeviceProperties.as_view(), name='getSingleDeviceProperties'),
    path('api/v1/devices/<int:device_id>/properties/<str:property_filter_field>', views.DeviceProperties.as_view(),name='getSingleDeviceProperty'),
]
