from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from .models import Properties, Devices
from rest_framework import generics
from .serializers import DevicesSerializer, PropertiesSerializer

from rest_framework.exceptions import NotFound

def index(request):
    return render(request,'api_doc.html')

class CreateDevice(generics.ListCreateAPIView):
    queryset = Devices.objects.all()
    serializer_class = DevicesSerializer

    def perform_create(self, serializer):
        try:

            serializer.save()
        except:
            HttpResponse(status=404)


class GetDevices(generics.ListAPIView):
    queryset = Devices.objects.all()
    serializer_class = DevicesSerializer


class DevicesRUD(generics.RetrieveUpdateDestroyAPIView):
    # this class can handle Single object RETURN, update and delete for the Device object
    queryset = Devices.objects.all()
    serializer_class = DevicesSerializer



class PropertiesRUD(generics.RetrieveUpdateDestroyAPIView):
    # this class can handle Single object RETURN, update and delete for the Properties object
    queryset = Properties.objects.all()
    serializer_class = PropertiesSerializer


class DeviceProperties(generics.ListAPIView):
    serializer_class = PropertiesSerializer
    lookup_url_kwarg = "device_id"

    def get_queryset(self):
        device_id = self.kwargs.get(self.lookup_url_kwarg)
        device_properties = Properties.objects.filter(DeviceId=device_id)
        query = self.request.GET.get("property_filter_field")

        if query is not None:
            device_properties = device_properties.filter(Q(name=query) |
                                                 Q(content=query) |
                                                 Q(type=query))

            if len(device_properties)>0:
                return device_properties
        else:
            if len(self.request.query_params)>0:
                raise NotFound()
            else:
                return device_properties


class CreateProperty(generics.ListCreateAPIView):
    queryset = Properties.objects.all()
    serializer_class = PropertiesSerializer

    def perform_create(self, serializer):
        try:

            serializer.save()
        except:
            HttpResponse(status=404)


class GetProperties(generics.ListAPIView):
    queryset = Properties.objects.all()
    serializer_class = PropertiesSerializer

    def get_queryset(self):
        queryset_list = Properties.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(Q(name=query) |
                                                 Q(content=query) |
                                                 Q(type=query))
        return queryset_list
