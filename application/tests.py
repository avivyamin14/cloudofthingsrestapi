import json
from audioop import reverse
from django.urls import reverse
from django.test import TestCase
from rest_framework import status
import unittest
from rest_framework.test import APIClient
from .models import Devices, Properties


class TestApplication(TestCase):
    def setUp(self):
        Devices.objects.create(pk=1, name="deviceTest1")
        Devices.objects.create(pk=2, name="deviceTest2")
        Devices.objects.create(pk=3, name="deviceTest3")
        Properties.objects.create(name="propertiesTest1", content="propertyContent1", type='propertyType1',
                                  DeviceId=Devices.objects.get(id=1))
        Properties.objects.create(name="propertiesTest2", content="propertyContent2", type='propertyType2',
                                  DeviceId=Devices.objects.get(id=1))
        Properties.objects.create(name="propertiesTest3", content="propertyContent3", type='propertyType3',
                                  DeviceId=Devices.objects.get(id=2))
        Properties.objects.create(name="propertiesTest4", content="propertyContent4", type='propertyType4',
                                  DeviceId=Devices.objects.get(id=2))

        self.client = APIClient()

    # DEVICES TESTS
    def test_create_device_succeess(self):
        url = reverse('createDevice')
        new_device = {"name": "goodAddedDevice"}
        response = self.client.post(url, new_device, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Devices.objects.get(pk=4).name, 'goodAddedDevice')
        self.assertEqual(json.loads(response.content), {"id": 4, "name": "goodAddedDevice"})
        Devices.objects.get(name='goodAddedDevice').delete()

    def test_create_device_fail(self):
        url = reverse('createDevice')
        new_bad_device = {"badDeviceFeild": "badDevice"}
        response = self.client.post(url, new_bad_device, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_GetDevices_success(self):
        url = reverse('getAllDevices')
        expected_data = [
            {"id": 1, "name": "deviceTest1"},
            {"id": 2, "name": "deviceTest2"},
            {"id": 3, "name": "deviceTest3"},
        ]
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), expected_data)
        self.assertNotEquals(json.loads(response.content), [])

    def test_get_single_device_success(self):
        url = reverse('getSingleDevice', kwargs={'pk': 3})
        response = self.client.get(url, format='json')
        expected_data = [{"id": 3, "name": "deviceTest3"}]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.load(response.content), expected_data)

    def test_get_single_device_fail(self):
        url = reverse('getSingleDevice', kwargs={'pk': 4})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_device_success(self):
        url = reverse('updateDevice', kwargs={'pk': 1})
        data_to_update = {"id": 2, "name": "DeviceTest2Updated"}
        response = self.client.patch(url, data=data_to_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.patch(url, data={"id": 2, "name": "deviceTest2"}, format='json')

    def test_update_device_fails(self):
        # updating an non exist device
        url = reverse('updateDevice', kwargs={'pk': 9})
        data_to_update = {"id": 9, "name": "DeviceNotExistUpdate"}
        response = self.client.patch(url, data=data_to_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # updating an exist device wrongly
        url = reverse('updateDevice', kwargs={'pk': 2})
        very_long_name = '1233445601230129310923812938912jadsidnsasijsdfnaishdjfnaishjbfnaihjbfnasihjbfaidhfbasihfbasdifajsdfnajsifnasijfansdifasdfni'
        data_to_update = {"id": "doest_matter", "name": very_long_name}
        response = self.client.patch(url, data=data_to_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(json.loads(response.content), {'name': ['Ensure this field has no more than 45 characters.']})

    def test_delete_device_success(self):
        url = reverse('deleteDevice', kwargs={'pk': 2})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.post(reverse('createDevice'), {"id": 2, "name": "deviceTest2"}, format='json')

    def test_delete_device_fails(self):
        # deleting an non existing device
        url = reverse('deleteDevice', kwargs={'pk': 4})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # PROPERTIES TESTS

    def test_get_properties_success(self):
        url = reverse('getAllProperties')
        response = self.client.get(url, format='json')
        expcted_data = [
            {"id": 1, "name": "propertiesTest1", "content": "propertyContent1", "type": "propertyType1", "DeviceId": 1},
            {"id": 2, "name": "propertiesTest2", "content": "propertyContent2", "type": "propertyType2", "DeviceId": 1},
            {"id": 3, "name": "propertiesTest3", "content": "propertyContent3", "type": "propertyType3", "DeviceId": 2},
            {"id": 4, "name": "propertiesTest4", "content": "propertyContent4", "type": "propertyType4", "DeviceId": 3}
        ]
        self.assertEqual(json.loads(response.content), expcted_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_property_success(self):
        url = reverse('getSingleProperty', kwargs={'pk': 1})
        response = self.client.get(url, format='json')
        expected_data = {"id": 1, "name": "propertiesTest1", "content": "propertyContent1", "type": "propertyType1",
                         "DeviceId": 1}
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), expected_data)

    def test_get_single_property_fails(self):
        # non exist property
        url = reverse('getSingleProperty', kwargs={'pk': 5})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_property_success(self):
        url = reverse('createProperty')

        new_property = {"id": 5, "name": "propertiesTest5", "content": "propertyContent5", "type": "propertyType5",
                        "DeviceId": 3}
        response = self.client.post(url, new_property, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Properties.objects.get(pk=5).name, 'propertiesTest5')
        self.assertEqual(json.loads(response.content), new_property)
        Properties.objects.get(name='propertiesTest5').delete()

    def test_create_property_fails(self):
        url = reverse('createProperty')
        # NOTE - if id given already exists it doesnt matter it will replace it with the +1 max id in the db model
        new_bad_property = {"id": 5, "BADFIELD": "propertiesTest5", "content": "propertyContent5",
                            "type": "propertyType5", "DeviceId": 3}
        response = self.client.post(url, new_bad_property, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_property_success(self):
        url = reverse('updateProperty', kwargs={'pk': 1})
        data_to_update = {"id": 1, "name": "UpdatedProperty", "content": "propertyContent5", "type": "propertyType5",
                          "DeviceId": 1}
        response = self.client.patch(url, data=data_to_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.patch(url, data={"id": 5, "name": "propertiesTest5", "content": "propertyContent5",
                                     "type": "propertyType5", "DeviceId": 3}, format='json')

    def test_update_property_fails(self):
        # updating an non exist device
        url = reverse('updateProperty', kwargs={'pk': 5})
        data_to_update = {"id": 9, "name": "DeviceNotExistUpdate"}
        response = self.client.patch(url, data=data_to_update, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # updating an exist device wrongly, error does not return error but returns the old object instead
        url = reverse('updateProperty', kwargs={'pk': 2})
        data_to_update = {"id": "doest_matter", "BADFIELD": 'blabla'}
        response = self.client.patch(url, data=data_to_update, format='json')
        self.assertEqual(json.loads(response.content),
                         {"id": 2, "name": "propertiesTest2", "content": "propertyContent2", "type": "propertyType2",
                          "DeviceId": 1})

    def test_delete_property_success(self):
        url = reverse('deleteProperty', kwargs={'pk': 3})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.post(reverse('createProperty'),
                         {"id": 3, "name": "propertiesTest2", "content": "propertyContent2", "type": "propertyType2",
                          "DeviceId": 1}, format='json')

    def test_delete_property_fails(self):
        # deleting an non existing property
        url = reverse('deleteProperty', kwargs={'pk': 4})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_single_device_properties_success(self):
        url = reverse('getSingleDeviceProperties', kwargs={'device_id': 1})
        response = self.client.get(url, format='json')
        print (self.client.request())
        expected_data = [
            {"id": 1, "name": "propertiesTest1", "content": "propertyContent1", "type": "propertyType1", "DeviceId": 1},
            {"id": 2, "name": "propertiesTest2", "content": "propertyContent2", "type": "propertyType2", "DeviceId": 1}
        ]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), expected_data)

    def test_get_single_device_properties_fails(self):
        # device with no properties == saerch for properties for not exist device id
        url = reverse('getSingleDeviceProperties', kwargs={'device_id': 3})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_single_device_property_success(self):
        url = '/application/api/v1/devices/1/properties/?property_filter_field=propertyContent1'
        response = self.client.get(url, format='json')
        expected_data = [{"id": 1, "name": "propertiesTest1", "content": "propertyContent1", "type": "propertyType1", "DeviceId": 1}]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),expected_data)


    def test_get_single_device_property_fails(self):
        url = '/application/api/v1/devices/1/properties/?BADVKEY=propertyContent1'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        url='/application/api/v1/devices/5/properties/?property_filter_field=propertyContent1'
        response = self.client.get(url, format='json')
        expected_data =[]
        self.assertEqual(json.loads(response.content),expected_data)

    if __name__ == '__main__':
        unittest.main()
