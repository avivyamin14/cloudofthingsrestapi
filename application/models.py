from django.db import models
import json


class Devices(models.Model):
    name = models.CharField(max_length=45)

    def __str__(self):
        device = json.dumps({'id': self.pk, 'name': self.name})
        return device


class Properties(models.Model):
    name = models.CharField(max_length=45)
    content = models.CharField(max_length=45)
    type = models.CharField(max_length=45)
    DeviceId = models.ForeignKey(Devices, on_delete=models.CASCADE,related_name='+')

    def __str__(self):
        property = json.dumps({'id': self.pk,'name': self.name, 'content': self.content, 'type': self.type})
        return property

